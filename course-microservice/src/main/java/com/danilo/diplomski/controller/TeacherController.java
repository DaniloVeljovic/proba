package com.danilo.diplomski.controller;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.danilo.diplomski.models.DTO.TeacherDTO;
import com.danilo.diplomski.models.kafka.TeacherKafkaModel;
import com.danilo.diplomski.services.TeacherService;


//broker kontroler
@Service
public class TeacherController {
	
	@Autowired
	private TeacherService teacherService;
	
	@KafkaListener(topics="second_topic", containerFactory="teacherKafkaListenerContainerFactory", groupId="group-one")
	public void createTeacher(TeacherKafkaModel newTeacher)
	{
		ModelMapper modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		
		TeacherDTO teacher = modelMapper.map(newTeacher, TeacherDTO.class);
		
		TeacherDTO createdTeacher = teacherService.createTeacher(teacher);
		
		System.out.println(createdTeacher);
		
		//TeacherResponseModel response = modelMapper.map(createdTeacher, TeacherResponseModel.class);
		
		//return new ResponseEntity<TeacherResponseModel>(response, HttpStatus.OK);
	}

}
