package com.danilo.diplomski.services.impl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.danilo.diplomski.exceptions.IllegalIdException;
import com.danilo.diplomski.models.DTO.CourseDTO;
import com.danilo.diplomski.models.DTO.ObligationDTO;
import com.danilo.diplomski.models.DTO.StudentDTO;
import com.danilo.diplomski.models.data.Course;
import com.danilo.diplomski.models.data.Obligation;
import com.danilo.diplomski.models.data.Student;
import com.danilo.diplomski.models.data.junctiontables.Registration;
import com.danilo.diplomski.repositories.StudentRepository;

import com.danilo.diplomski.services.StudentService;

@Service
public class StudentServiceImpl implements StudentService {

	private StudentRepository studentRepo;

	@Autowired
	public StudentServiceImpl(StudentRepository studentRepo) {
		this.studentRepo = studentRepo;
	}

	@Override
	public StudentDTO createStudent(StudentDTO studentToCreate) {

		try {
			ModelMapper modelMapper = new ModelMapper();
			modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);

			Student student = modelMapper.map(studentToCreate, Student.class);

			student.setUserID(UUID.randomUUID().toString());

			Student saved = studentRepo.save(student);

			StudentDTO response = modelMapper.map(saved, StudentDTO.class);

			return response;
		} catch (Exception e) {
			throw new RuntimeException("Error in POST /students " + e.getMessage());
		}
	}

	@Override
	public List<CourseDTO> findAllCoursesForStudent(String studentID) {
		try {

			List<CourseDTO> response = new ArrayList<CourseDTO>();
			ModelMapper modelMapper = new ModelMapper();
			modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);

			Optional<Student> student = studentRepo.findByUserID(studentID);

			if (!student.isPresent())
				throw new IllegalIdException("Student with ID" + studentID + "not found");

			Set<Registration> courses = student.get().getRegistrations();

			for (Registration r : courses) {
				Course c = r.getCourses();

				response.add(modelMapper.map(c, CourseDTO.class));
			}

			return response;

		} catch (Exception e) {
			throw new RuntimeException("Error in POST /students " + e.getMessage());
		}
	}

	@Override
	public List<ObligationDTO> findAllObligationsForDate(String studentID, LocalDateTime date) {
		try {
			Student student = studentRepo.findByUserID(studentID).get();

			ModelMapper modelMapper = new ModelMapper();
			modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);

			Set<Registration> regs = student.getRegistrations();

			List<ObligationDTO> returnObligat = new ArrayList<ObligationDTO>();

			for (Registration r : regs) { // iskoristi hes mapu ovde, bice brze
				Set<Obligation> oblig = r.getCourses().getObligations();

				for (Obligation o : oblig) {
					// if (o.getDate().compareTo(date) == 0) {
					if ((o.getDate().getDayOfMonth() == date.getDayOfMonth())
							&& (o.getDate().getMonth().equals(date.getMonth()))
							&& (o.getDate().getYear() == date.getYear())) {
						returnObligat.add(modelMapper.map(o, ObligationDTO.class));
					}

				}
			}

			return returnObligat;
		} catch (Exception e) {
			throw new RuntimeException("Error in POST /students " + e.getMessage());
		}
	}

	@Override
	public Student findByUserID(String studentID) {

		Optional<Student> student = studentRepo.findByUserID(studentID);

		if (!student.isPresent())
			throw new IllegalIdException("Student with ID" + studentID + "not found");

		return student.get();

	}

}
//create student
/*
 * try { Student studentToSave = studentRepo.save(student);
 * 
 * return new ResponseEntity<Student>(studentToSave, HttpStatus.OK); } catch
 * (Exception ex) { throw new
 * RuntimeException("Error in POST /students . Message: " + ex.getMessage()); }
 */

//get all courses in which the student enrolled
/*
 * try { Iterable<Registration> registratedCoursesForStudent =
 * registrationRepo.findByStudentId(idStudent); List<Course> foundCourses = new
 * ArrayList<Course>();
 * 
 * for (Registration r : registratedCoursesForStudent) {
 * foundCourses.add(r.getCourses()); }
 * 
 * List<CourseResponseModel> response =
 * courseService.convertToIterableDTOResponses(foundCourses);
 * 
 * return new ResponseEntity<List<CourseResponseModel>>(response,
 * HttpStatus.OK); } catch (Exception ex) { throw new RuntimeException(
 * "Error in POST /students/+" + idStudent + "/course/" + idCourse +
 * ". Message: " + ex.getMessage()); }
 */

// retrieve all obligations for a student for a specific date
/*
 * try { Student student = studentRepo.findById(id).get();
 * 
 * Set<Registration> regs = student.getRegistrations();
 * 
 * List<ObligationDTO> returnObligat = new ArrayList<ObligationDTO>();
 * 
 * for (Registration r : regs) { // iskoristi hes mapu ovde, bice brze
 * Set<Obligation> oblig = r.getCourses().getObligations();
 * 
 * for (Obligation o : oblig) { if (o.getDate().compareTo(date) == 0) {
 * 
 * // OVO POMERI U OBLIGATION SERVIS
 * 
 * ObligationDTO tempObligation = new ObligationDTO();
 * tempObligation.setDate(o.getDate()); tempObligation.setId(o.getId());
 * tempObligation.setDescription(o.getDescription());
 * tempObligation.setType(o.getType()); returnObligat.add(tempObligation); }
 * 
 * } }
 * 
 * return new ResponseEntity<List<ObligationDTO>>(returnObligat, HttpStatus.OK);
 * 
 * } catch (Exception ex) { throw new
 * RuntimeException("Error in GET /students//{id}/date/{date} " + "Message: " +
 * ex.getMessage()); }
 */