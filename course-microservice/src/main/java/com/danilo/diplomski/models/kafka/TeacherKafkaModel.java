package com.danilo.diplomski.models.kafka;

public class TeacherKafkaModel {

	private String userID;
	private String role;
	private String name;

	public TeacherKafkaModel() {

	}

	public TeacherKafkaModel(String userID, String role, String name) {
		this.userID = userID;
		this.role = role;
		this.name = name;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "TEACHER [userID=" + userID + ", role=" + role + ", name=" + name + "]";
	}
	
	

}
