package com.danilo.diplomski.services.impl;

import java.util.Optional;
import java.util.UUID;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.danilo.diplomski.exceptions.IllegalIdException;
import com.danilo.diplomski.models.DTO.TeacherDTO;

import com.danilo.diplomski.models.data.Teacher;
import com.danilo.diplomski.repositories.TeacherRepository;
import com.danilo.diplomski.services.TeacherService;

@Service
public class TeacherServiceImpl implements TeacherService {

	private TeacherRepository teacherRepo;

	@Autowired
	public TeacherServiceImpl(TeacherRepository teacherRepo) {
		this.teacherRepo = teacherRepo;
	}

	@Override
	public TeacherDTO createTeacher(TeacherDTO teacher) {
		try {
			ModelMapper modelMapper = new ModelMapper();
			modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);

			Teacher teacherCreate = modelMapper.map(teacher, Teacher.class);

			teacher.setUserID(UUID.randomUUID().toString());

			Teacher saved = teacherRepo.save(teacherCreate);

			TeacherDTO response = modelMapper.map(saved, TeacherDTO.class);

			return response;
		} catch (Exception e) {
			throw new RuntimeException("Error in POST /students " + e.getMessage());
		}
	}

	// ova i student service impl find by user id treba da vraca dto, ali ne mogu da
	// konvertujem vise
	@Override
	public Teacher findByUserID(String teacherID) {

		Optional<Teacher> teacher = teacherRepo.findByUserID(teacherID);

		if (!teacher.isPresent())
			throw new IllegalIdException("Student with ID" + teacherID + "not found");

		return teacher.get();

	}

}
